package com.company;
public class ContractEmployee extends Employee{
    public final int federalTaxIDmember;

    public ContractEmployee(int employeeID, String lastName, String firstName, int age, String education, int federalTaxIDmember) {
        super(employeeID, lastName, firstName, age, education);
        this.federalTaxIDmember = federalTaxIDmember;
    }

    public double calculatePay(){
        final int numStandard = this.federalTaxIDmember * 10050;
        double pay = numStandard / 500.10;
        return pay;
    }

}

