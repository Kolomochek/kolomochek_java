package com.company;
import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {

        Employee emp1 = new ContractEmployee(1, "Surname: Moon",
                "Name: Nadya", 15, "Education: ABC University", 88888);
        Employee emp2 = new ContractEmployee(2, "Surname: Bae",
                "Name: Ruslan", 24, "Education: HHJ University", 10876);
        Employee emp3 = new ContractEmployee(3, "Surname: Ji",
                "Name: Vanya", 54, "Education: TyJ University", 36443);
        Employee emp4 = new ContractEmployee(4, "Surname: Choi",
                "Name: Vlad", 77, "Education: VVN University", 98075);

        Employee emp5 = new SalariedEmployee(5, "Surname: Kim",
                "Name: Vika", 17, "Education: CL University", 6789);
        Employee emp6 = new SalariedEmployee(6, "Surname: Lee",
                "Name: Chonsok", 29, "Education: QW University", 9090);
        Employee emp7 = new SalariedEmployee(7, "Surname: Hyuning",
                "Name: Bahie", 43, "Education: KFC University", 2311);
        Employee emp8 = new SalariedEmployee(8, "Surname: Lee",
                "Name: Felix", 66, "Education: MSB University", 4332);

        List<ContractEmployee> employeesPlayM = new ArrayList<>();
        employeesPlayM.add((ContractEmployee) emp1);
        employeesPlayM.add((ContractEmployee) emp2);
        employeesPlayM.add((ContractEmployee) emp3);
        employeesPlayM.add((ContractEmployee) emp4);

        List<SalariedEmployee> employeesPlayM2 = new ArrayList<>();
        employeesPlayM2.add((SalariedEmployee) emp5);
        employeesPlayM2.add((SalariedEmployee) emp6);
        employeesPlayM2.add((SalariedEmployee) emp7);
        employeesPlayM2.add((SalariedEmployee) emp8);

        Activity playMEnt = new Activity(employeesPlayM, employeesPlayM2);
        playMEnt.info();
        System.out.println(playMEnt.getPayments(emp1));
        System.out.println(playMEnt.getPayments(emp2));
        System.out.println(playMEnt.getPayments(emp3));
        System.out.println(playMEnt.getPayments(emp4));
        System.out.println(playMEnt.getPayments(emp5));
        System.out.println(playMEnt.getPayments(emp6));
        System.out.println(playMEnt.getPayments(emp7));
        System.out.println(playMEnt.getPayments(emp8));

      }
    }


