package com.company;
public class SalariedEmployee extends Employee{
    public final int socialSecurityNumber;

    public SalariedEmployee(int employeeID, String lastName, String firstName, int age, String education, int socialSecurityNumber) {
        super(employeeID, lastName, firstName, age, education);
        this.socialSecurityNumber = socialSecurityNumber;
    }

    public double calculatePay(){
        final int numStandard = this.socialSecurityNumber * 5700;
        double pay = numStandard / 70.17;
        return pay;
    }

}


