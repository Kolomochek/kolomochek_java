public class Bird {

    protected String name;
    protected String food;
    protected String area;
    protected String eat;
    protected String fly;
    protected String run;
    protected String cry;
    protected String swim;

    public Bird(String name, String food, String area, String eat, String fly, String run, String cry, String swim){
        this.name = name;
        this.food = food;
        this.area = area;
        this.eat = eat;
        this.fly = fly;
        this.run = run;
        this.cry = cry;
        this.swim = swim;
    }

    public String getName(){
        return name;
    }
    public void setName(String name){
        this.name = name;
    }
    public String getFood(){
        return food;
    }
    public void setFood(String food){
        this.food = food;
    }
    public String getArea(){
        return area;
    }
    public void setArea(String area){
        this.area = area;
    }
    public String getEat(){
        return eat;
    }
    public void setEat(String eat){
        this.eat = eat;
    }
    public String getFly(){
        return fly;
    }
    public void setFly(String fly){
        this.fly = fly;}
    public String getRun(){
        return run;
    }
    public void setRun(String run){
        this.run = run;
    }
    public String getCry(){
        return cry;
    }
    public void setCry(String cry){
        this.cry = cry;
    }
    public String getSwim(){
        return swim;
    }
    public void setSwim(String swim){
        this.swim = swim;
    }
}
