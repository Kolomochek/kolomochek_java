import java.util.*;

public class Activity {

    private static final Scanner in = new Scanner(System.in);

    public void printMenu(){
        System.out.println("1 - Swallow-");
        System.out.println("2 - Eagle-");
        System.out.println("3 - Penguin-");
        System.out.println("4 - Kiwi-");
        System.out.println("5 - Duck-");
        System.out.println("6 - Ostrich-");
        System.out.println("7 - Exit");
    }

    public void initActivity() {

        ArrayList<Bird> birds = new ArrayList<>();
        Swallow swallow = new Swallow ("Swallows ", "insectivorous. ", "widespread in Europe, North Africa and temperate latitudes of Asia. ", "catches insects in the air. ", "Can fly. ", "Cant run. ", "Swallows are able to produce many different calls or songs, which are used to express excitement, to communicate with others of the same species, during courtship, or as an alarm when a predator is in the area. ", "Cant swim. \n");
        Eagle eagle = new Eagle("Eagles ", "feed on small and medium-sized vertebrates, sometimes carrion. ", "distributed in Eurasia, Africa and North America from forest tundra to deserts. ", "Look out for prey, hovering in the air, or watch out, sitting in an elevated place. ", "Can fly. ", "Can run. ", "Can cry. ", "Some species of eagles can swim. \n");
        Penguin penguin = new Penguin("Penguins ", "feed on fish and crustaceans. ", "Penguins live in the open seas of the Southern Hemisphere: in the coastal waters of Antarctica, New Zealand, southern Australia, South Africa, along the entire coast of South America from the Falkland Islands to Peru, on the Galapagos Islands near the equator. ", "Dive and swallow fish underwater. ", "Cant fly. ", "Can run. ", "Can cry. ", "Can swim. \n ");
        Kiwi kiwi = new Kiwi("Kiwis ", "food consists of insects, molluscs and earthworms, as well as fallen berries and fruits.", "Kiwis are only found in New Zealand. ", "Kiwi prey is searched for with the help of smell and touch - raking the ground with their feet and deeply immersing their beak into it, they literally \"sniff out' worms and insects\"", "Cant fly. ", "Can run. ", "Can cry. ", "Can swim. \n");
        Duck duck = new Duck("Ducks ", "food is vegetable food, but in many cases animal food also predominates.  ", "Ducks are common all over the world, absent only in Antarctica and on some oceanic islands. ", "Catch fish in lakes or eat from feeder. ", "Can fly. ", "Can run. ", "Ducks quack.", "Can swim. \n");
        Ostrich ostrich = new Ostrich("Ostriches ", "feed on buds, flowers and small fruits of plants, catch small animals - insects, lizards, small turtles. ", "live in the savannas and semi-deserts of Africa", "They swallow small stones to grind food in their stomachs. ", "Cant fly. ", "Can run. ", "Can Cry. ", "Can swim \n");

        birds.add(swallow);
        birds.add(eagle);
        birds.add(penguin);
        birds.add(kiwi);
        birds.add(duck);
        birds.add(ostrich);

        while (true){
            printMenu();
            int i = in.nextInt();
            if (i == 1){
                System.out.println(swallow.getName() + "are " + swallow.getFood() + "They " + swallow.getArea() + "She " + swallow.getEat() + swallow.getFly() + swallow.getRun() + swallow.getCry() + swallow.getSwim() );
            }else if (i == 2) {
                System.out.println(eagle.getName() + eagle.getFood() + "They " + eagle.getArea() + eagle.getEat()+ eagle.getFly()+ eagle.getRun()+eagle.getCry()+eagle.getSwim());
            }else if (i == 3) {
                System.out.println(penguin.getName() + penguin.getFood() + "They " + penguin.getArea() + penguin.getEat() + penguin.getFly() + penguin.getRun() + penguin.getCry() + penguin.getSwim());
            }else if (i == 4) {
                System.out.println(kiwi.getName() + "Their" + kiwi.getFood() + kiwi.getArea() + kiwi.getEat() + kiwi.getFly() + kiwi.getRun() + kiwi.getCry() + kiwi.getSwim());
            }else if (i == 5) {
                System.out.println(duck.getName()+duck.getFood() + duck.getArea() + duck.getEat()+ duck.getFly()+ duck.getRun()+duck.getCry()+duck.getSwim());
            }else if (i == 6) {
                System.out.println(ostrich.getName()+ostrich.getFood() + "They " + ostrich.getArea() + ostrich.getEat()+ ostrich.getFly()+ ostrich.getRun()+ostrich.getCry()+ostrich.getSwim());
            }
            else if (i == 7){break;}
        }
    }
}
